# Helmholtz Cloud Service Operation KPI

## Preambel
**HIFIS - Helmholtz Digital Services for Science — Collaboration made easy.**
[HIFIS](https://hifis.net/) – the Helmholtz Federated IT Services – supports scientific projects with IT resources, services, cloud services, consulting, and expertise from the collection, storage, linking, and analysis of research data to the publication of the results. HIFIS also supports the development of well-designed research software.
This includes the [Helmholtz Cloud](https://helmholtz.cloud), where members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.

## Purpose
This is to document the calculcation of the **Helmholtz Cloud Service Operation KPI** (WP 1.4) for Helmholtz Cloud Services by the HIFIS Cloud Cluster Management of HIFIS. For further information, see [HIFIS homepage](https://hifis.net) and [Original platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf), Page 56, about WP 1.4.

## Regular Measurement Series and Calculation
Each [Helmholtz Cloud Service](https://helmholtz.cloud/services/) determines **regular measurement series** which reflect the current usage. The measured raw values indicate how much the individual service is used. Accordingly, the character of the measurement series depends on the character of the service and varies from service to service.

- All steps of the calculation are publicly available in a [**public repository**](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci), i.e. scripts, weighting, intermediate results, final result. The full history is accessible.
- The [**calculation is described in detail here**](kpi-calculation.md).
- The [daily updated results and plots are available here](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/browse/plots_kpi?job=plot_general).
KPI previsouly reported for specific purposes are [summarized below](#published-kpi-results).

## Interpretation of the KPI
Generally, the KPI represent both individual users (50%) and resource usage (50%) of Helmholtz Cloud services.
Positive KPI can be interpreted as increased usage in relation to when the service was added to the Helmholtz Cloud; negative values would indicate decreased usage.
Addition of more services (# services) and/or increased number of users per service and/or increased usage of services lead to an increasing KPI.

Therefore, one can interpret the KPI as follows:

- Overall KPI (third column below) indicate the **quantitative** development of the Helmholtz Cloud services over time.
- Overall KPI divided by numbers of services (fourth and fifth column below) indicate the **qualitative** development of the Helmholtz Cloud Services, i.e., the average user acceptance per service over time.

## Published KPI Results

Date | Context | KPI | # services | KPI / service | Documentation | All Sources | Detailed Results
-- | -- | -: | :-: | :-: | -- | -- | --
2021-12-31 | [Annual report '21](https://nubes.helmholtz-berlin.de/s/o3f4sN3bpD5oDLm/download) | [3.39](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/537267/artifacts/file/plots_kpi/overall-kpi.csv) | [7](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/3d7d34927ba800cffc5c9bcf50745ef368c40225/data/kpi-weights.csv) | 0.48 | [Link to Doc](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/blob/34371cf3bf5fe5bb62fa40f1943bd8fe73a7823e/docs/cloud-service-kpi/kpi-calculation.pdf) | [Link to Sources](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/tree/3d7d34927ba800cffc5c9bcf50745ef368c40225) | [Link to Results](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/537267/artifacts/browse/plots_kpi/)
2022-05-31 | [SAB '22](https://events.hifis.net/event/417/)  | [6.63](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/613782/artifacts/file/plots_kpi/overall-kpi.csv) | [7](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/6b9ef53ce4244b93960cace82a7d5bbfd5e2a9dc/data/kpi-weights.csv) | 0.95 | [Link to Doc](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/blob/34371cf3bf5fe5bb62fa40f1943bd8fe73a7823e/docs/cloud-service-kpi/kpi-calculation.pdf) | [Link to Sources](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/tree/6b9ef53ce4244b93960cace82a7d5bbfd5e2a9dc/) | [Link to Results](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/613782/artifacts/browse/plots_kpi/)
2022-06-30 | Review report '22 | [12.23](https://hifis.pages.hzdr.de/-/overall/kpi/kpi-plots-ci/-/jobs/660098/artifacts/plots_kpi/overall-kpi.txt) | [11](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/cbb0d95a047e629d621a79304e2f5dc2cffeb2dc/data/kpi-weights.csv) | 1.11 | [Link to Doc](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/blob/44ea091e726efde16f356c9e0e0198e9aedbdc4e/docs/cloud-service-kpi/kpi-calculation.md) | [Link to Sources](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/tree/cbb0d95a047e629d621a79304e2f5dc2cffeb2dc) | [Link to Results](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/660098/artifacts/browse/plots_kpi/)
2022-12-31 | [Annual report '22](https://nubes.helmholtz-berlin.de/f/574934254) | [19.69](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/893754/artifacts/external_file/plots_kpi/overall-kpi.txt) | [15](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/69adcb90b20bdca6097f2047a900aa9b7cd64d4c/kpi-weights.csv) | 1.31 | [Link to Doc](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/blob/1a746bf9c3c8e1dfb15764da9e99d5027eac521a/docs/cloud-service-kpi/kpi-calculation.md) | [Link to Sources](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/tree/69adcb90b20bdca6097f2047a900aa9b7cd64d4c) | [Link to Results](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/893754/artifacts/browse/results/)
