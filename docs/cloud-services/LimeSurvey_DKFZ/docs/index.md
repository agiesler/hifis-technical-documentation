# Welcome to HIFIS LimeSurvey Documentation

LimeSurvey is an online Survey Service offered by DKFZ for use to the members of the [Helmholtz Association](https://www.helmholtz.de) under Helmholtz AAI services.

## Contact

* For any help regarding LimeSurvey, write to [**Support**](mailto:support@hifis.net?subject=[lime-survey-dkfz] - Support Request).

## Mailing  List

* Await important notifications about maintainance activities, new features from our mailer  **hifis-limesurvey-dkfz-announce@dkfz-heidelberg.de**

To Subscribe [**Click Here**](mailto:hifis-limesurvey-dkfz-announce-join@dkfz-heidelberg.de?subject=Subscribe to mailing list)

And while we are sorry to see you go, to Unsubscribe  [**Click Here**](mailto:hifis-limesurvey-dkfz-announce-join@dkfz-heidelberg.de?subject=Unsubscribe from mailing list)


## FAQs

* **Forgot Password** <br/>

The password cannot be reset using the link on login page, as of now this can only be done at home institute.

* **How to Sign-up for HIFIS LimeSurvey?** <br/>

Reach out to [**Support**](mailto:support@hifis.net?subject=[lime-survey-dkfz] - Sign-up Request). A Data Protection Agreement is to be set up with each partner institute. For enabling the service, a Project based VO or a Sub VO to an existing Project based VO can be used. If you need help with creating a Project based VO, please refer to the corresponding [documentation](../../../helmholtz-aai/howto-vos.md).

* **How to create survey?** <br /> <br />
After login, on the top right corner of the page. <br /> 
![Placeholder](./pictures/select_create.png) <br /> 
<br /> 
<br />

Click on the arrow next to **Surveys** button. Select **Create a new survey**. <br />
![Placeholder](./pictures/create_survey.png) <br /> 
<br /> 
<br />

Give the desired name and fill out other relevant details before clicking on **SAVE**. <br />
![Placeholder](./pictures/entries.png) <br /> 
<br /> 
<br />

* **How to provide survey permission?** <br />

Go to list of surveys. <br /> <br />
![Placeholder](./pictures/list_of_survey.png) <br /> <br /> <br />

Click on the survey you wish to select. <br /> <br />
![Placeholder](./pictures/survey_selection.png) <br /> <br />
<br />

On the left scroll down to Survey Permissions. <br /> <br />
![Placeholder](./pictures/survey_permission.png) <br /> <br />
<br />

Click on the drop down for users to provide access to a particular user. <br /> <br />
![Placeholder](./pictures/user_list.png) <br /> <br />
<br />

Click on drop down for group to select which group should have access to the survey. <br /> <br />
![Placeholder](./pictures/group_list.png) <br /> 
<br />
<br />

* **How to publish a survey?** <br />

From the list of surveys, Select the desired survey. <br /> <br />
![Placeholder](./pictures/list_of_survey.png) <br /> 
<br /> 
<br />


Click on **Activate this survey** on the top menu. <br /> <br />
![Placeholder](./pictures/activate1.png) <br /> <br /> <br />

Verify entries and click on **Save & activate survey**. <br /> <br />
![Placeholder](./pictures/activate2.png) <br /> <br /> <br />

* **Should Privacy Policy be included in Survey?** <br />

---See German Version Below---

Whether personal data is collected in a survey is up to the survey creator responsible for the survey. The survey creator must provide information about their processing on the homepage of the survey and, if necessary, obtain a declaration of consent. With the general data privacy statement on the website, DKFZ fulfills its duty to inform pursuant to Article 13 of the EU General Data Protection Regulation (EU GDPR) and informs survey participants about the data processing of data collected beyond the survey form (e.g. browser cookies (e.g. to exclude repeated participation, date stamps, etc.).
Whether and to what extent a privacy statement is necessary within the survey depends on the survey in question and should be coordinated with the data protection officer of the relevant institution.

----------------------------------------------------------------

Ob in einer Befragung personenbezogene Daten erhoben werden, obliegt dem für die Umfrage verantwortlichen Umfrageersteller. Über deren Verarbeitung muss der Umfrageersteller auf der Startseite der Umfrage informieren und gegebenfalls eine Einwilligungserklärung einholen. Mit der allgemeinen Datenschutzerklärung auf der Website kommt das DKFZ seiner Informationspflicht gemäß Artikel 13 der EU-Datenschutzgrundverordnung (EU-DSGVO) nach und informiert Umfrageteilnehmer über die Datenverarbeitung von Daten, die über das Umfrageformular hinaus erhoben werden (z.B. Browser-Cookies (z.B. um wiederholte Teilnahme auszuschließen, Datumstempel etc.).

Ob und in welchem Umfang eine Datenschutzerklärung innerhalb der Umfrage notwendig ist, hängt von der jeweiligen Umfrage ab und sollte mit dem Datenschutzbeauftragten der entsprechenden Einrichtung abgestimmt werden.

## Technical Documentation

* [AAI Integration](https://codebase.helmholtz.cloud/hifis/cloud/si/limesurvey)

## Technical Requirements

The following attributes must be provided on login via Helmholtz AAI

* eduPersonEntitlement:<br/>
    * A Project Based VO or Sub VO to an existing Project Based VO<br/>
    e.g. `urn:geant:helmholtz.de:group:MyNiceProject` OR `urn:geant:helmholtz.de:group:MyProject:SubProject`<br/><br/>

* eduPersonScopedAffiliation:<br/>
    * e.g. `staff@dkfz-heidelberg.de`<br/><br/>

* eduPersonAssurance (exact match):<br/>
    * `https://refeds.org/assurance/ATP/ePA-1m`<br/>
    * `https://refeds.org/assurance/ID/unique`<br/>

## Imprint

Herausgeber<br/>
Anbieter dieses Online-Umfragesystems ist das Deutsche Krebsforschungszentrum (DKFZ)


Anschrift<br/>
Deutsches Krebsforschungszentrum<br/>
Im Neuenheimer Feld 280<br/>
69120 Heidelberg<br/>
Telefon: +49 (0)6221 420<br/>
Internet: www.dkfz.de<br/>
E-Mail: kontakt@dkfz.de<br/>


Rechtsform<br/>
Das Deutsche Krebsforschungszentrum ist eine Stiftung des öffentlichen Rechts des Landes Baden-Württemberg. Gültig ist die Satzung vom August 2014.

Umsatzsteuer-Identifikationsnummer: DE 143293537


Vertretungsberechtigte Personen<br/>
Prof. Dr. Michael Baumann (Vorstandsvorsitzender und Wissenschaftlicher Vorstand)<br/>
Ursula Weyrich (kaufmännischer Vorstand)<br/>

(Anschriften wie oben)


System Verantwortlich<br/>
Die Information Technology Core Facility des Deutschen Krebsforschungszentrum.<br/>


Inhaltlich Verantwortlich<br/>
Für die Inhalte von einzelnen Umfragen ist der jeweilige Umfrage-Administrator verantwortlich.


## Privacy Policy

This policy is effective from 01.07.2021. <br/>

Privacy Policy

| Title                                          | Description                                    | 
| ---------------------------------------------- | ---------------------------------------------- | 
|1. Name of the Service | HIFIS LimeSurvey DKFZ  |
|2. Description of the Service | HIFIS LimeSurvey DKFZ is an online survey service offered by DKFZ for use to Helmholtz centres.| 
|3. Data Controller and a contact person | German Cancer Research Center - Foundation under public law <br/> Im Neuenheimer Feld 280 <br/> 69120 Heidelberg <br/> Germany <br/> Telephone: +49 (0)6221 420 <br/> Email: kontakt@dkfz.de <br/> Website: www.dkfz.de| 
|4. Data Controller’s data protection officer (if applicable) | Data Protection Officer <br/> German Cancer Research Center - Foundation under public law <br/> Im Neuenheimer Feld 280 <br/> 69120 Heidelberg <br/> Telephone: +49 (0)6221 420 <br/> Email: datenschutz@dkfz.de <br/>|
|5. Jurisdiction and supervisory authority  | DE, Germany, Baden-Württemberg <br/> Notwithstanding any other administrative or judicial remedies, you also have the right to lodge a complaint wit a supervisory authority, in particular in the Member State where you reside  or work or where the alleged violation takes place if you believe that the processing of your personal data is in violation of GDPR. <br/> <br/> The supervisory authority to which the complaint is submitted informs the complainant on the status and the results of the complaint including the possibility of a judicial remedy as pursuant to Article 78 GDPR.|
|6. Personal data processed and the legal basis  | A. Personal data retrieved from your Home organisation: <br/> ● Pseudonymous identifier* <br/> ● Your Affiliation in your home organization, based on eduperson schema* <br/> ● Assurance information of your home organization, based on eduperson schema* <br/> ● Your Email Address <br/> ● Your full name, based on eduperson schema* <br/> B. Personal data gathered from yourself <br/> ● Pseudonymous identifier <br/> ● eduPerson Scoped Affiliation (1) (2) (3) <br/> ● eduPerson Assurance (1) (2) (3) <br/>● Displayname <br/> ● Email <br/> ● eduPerson Entitlement (1) (2) |
|7. Purpose of the processing of personal data   |This data is necessary for account management purposes (e.g. to contact you to inform you of changes to the service or for security purposes), and for the reasons given in the paragraph below. Log records of your access to and actions on HIFIS LimeSurvey DKFZ are retained. These records contain: <br/> ● Pseudonymous identifier <br/> ● The network (IP) address from which you access HIFIS LimeSurvey DKFZ <br/> ● Time and date of access <br/> ● Details of actions you perform <br/> This data is necessary to ensure that the HIFIS LimeSurvey DKFZ service is reliable and secure, such as for assisting in the analysis of reported problems, contacting you if a problem is identified with your account and responding to security incidents. This data may also be used for authorised services acting on behalf of authorised users. |
|8. Third parties to whom personal               | Your data will not be disclosed to third parties. |
|9. How to access, rectify and delete the <br/> personal data and object to its processing   | Contact the contact personal above. To rectify the data released by your Home Organisation, contact your Home Organisation’s IT helpdesk.|
|10. Withdrawal of consent                       | You have the right to revoke your consent to data processing at any time. Upon revoking consent, the legality of the data processing carried out on the basis of the consent will not be affected by the revocation of consent. This can be done by contacting support@hifis.net |
|11. Data portability                            | You have the right to receive the personal data concerning you which you have provided to a controller, in a structured, commonly used and machine-readable format. Moreover, you also have the right to transmit those data to another controller without hindrance from the controller to which the personal data have been provided in sofar as <br/>(1) the processing is based on a granted consent as pursuant to Article 6 (1) lit. a) GDPR or Article 9 (2) lit. a) GDPR or on a contract pursuant to Article 6 (1) lit. b) GDPR and <br/> (2) the processing is carried out by automated means. In exercising this right, you also have the right to have the personal data concerning you transmitted directly from one controller to another, wherever technically feasible. This may not adversely affect the rights and freedoms of others. <br/> The right to data portability does not apply to the processing of personal data if it is required for the performance of a task carried out in the public interest or in the exercise of official authority vested in the controller. |
|12. Data retention                              | Personal data is deleted on request of the user or if the user hasn't used the service for 6 months.|
|13. Data Protection Code of Conduct             | Your personal data will be protected according to the Code of Conduct for Service Providers [1], a common standard for the research and higher education sector to protect your privacy. | 


REFERENCES <br/>

[1] GÉANT Data Protection Code of Conduct - http://www.geant.net/uri/dataprotection-code-of-conduct/v1
