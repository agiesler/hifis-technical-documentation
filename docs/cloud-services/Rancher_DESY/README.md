# Rancher Managed Kubernetes by DESY

The DESY-hosted Rancher Service allows [Rancher managed Container Orchestration on Kubernetes Cluster](https://rancher.com/why-rancher),
based on the Openstack Cloud Provider. See [rancher.com](https://rancher.com/) for more details on the concept and benefits. 

DESY provides access to an existing guest-cluster that can be used for anything between trying out Kubernetes and its concepts up to deploying applications for testing and review. 
Each user or user group from a Helmholtz centre or related institute can apply for access with a short description of their intentions and motivation for using the cluster.
We will create a project for each user or user group in such a way that all projects existing on the cluster are isolated from one another.
Thus, the different tenants will not interfere with each other, making it safe to use for everyone.

## How to get access
If you want to have access to the cluster, have any questions/issues regarding this service or want to find out if this offer is right for you and your use case, feel free to [**contact us**](mailto:support@hifis.net?subject=%5Bdesy-compute%5D%20-%20Support%20Request).
We will be in touch as soon as possible and usually arrange for a quick video call as we feel that this is the best way to get to know you and your use case.

## More links and resources
* [Rancher documentation](https://docs.ranchermanager.rancher.io/)

## Q&A

#### How do I get access to the cluster in general?  
Please have a look at the [service description in the Helmholtz Cloud Portal](https://helmholtz.cloud/services/?serviceID=8da9d670-383c-4641-9896-fa25220cc0b5).
There you will find all the information you need to request access to the service.

#### How can I access the cluster to deploy my application?  
In multiple ways: You can log into the WebUI at rancher.desy.de and use it right away. There you can download credentials to access the cluster either via the dedicated `rancher` or the more generic `kubectl` CLI applications. Both are small binaries that act as wrappers around the respective APIs.

#### Can I access an application deployed on the guest cluster with my browser?  
Yes, we have an nginx-ingress-controller in the cluster that allows accessing e.g. websites on ports 80 and 443 in the cluster. It is registered for guest-cluster2-ingress.desy.de.

#### Can I get a DNS entry for my application?  
Yes, but not a desy.de entry. For testing purposes you can use the service at nip.io, which refers to the IP you give it in front of nip.io, i.e. `something.131.169.234.237.nip.io` or `something-83a9eabf.nip.io`, where `83a9eabf` is a hexadecimal representation of the IP mentioned before. You can also get a more permanent DNS entry at HZB (open an issue in this [repository](https://codebase.helmholtz.cloud/hifis/cloud/domains) to apply for it).

#### Can I get certificates for SSL/TLS connections to port 443 of my application?  
Yes, we deployed `cert-manager` for exactly this purpose. It enables you to get a domain-validated X509 certificate from Let's Encrypt (staging and production). You can also apply for a certificate for `YOURSUBDOMAIN.helmholtz.cloud` from HZB.

#### Can I use the gitlab-agent to deploy applications directly from my gitlab repository?  
Unfortunately not, due to the form of role based access control (RBAC) used by Rancher and Kubernetes. We are working on a way to enable the gitlab-agent with the correct RBAC for everyone, though.
