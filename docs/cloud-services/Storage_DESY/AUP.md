# HIFIS Storage (dCache) AUP, provided by DESY

## Acceptable Use Policy

This document is the Acceptable Use Policy (AUP) for the "HIFIS Storage (HDF)" service based on dCache (the service). It lists what
we (the people providing the storage service) expect from you (the
scientist) when you use this storage service, either for storing your
data or when accessing any stored data.

1.  Scientific research.
    The service is provided to support scientific research.  Please do not
    use it to store your family photos, music or movie collection, or
    anything else that is not directly related to your scientific
    research.

2.  Only for legal activity
    Only use the service for legal activity.  Do not store any illegal
    content and do not use the service to support any illegal activity.

3. You are responsible for the contents

    Some data is sensitive must be stored carefully.  Examples of such
    data include medical records, business records or anything that
    requires confidentiality.  Never store such data at dCache.


4. You are responsible for the people you invite

    The service provides mechanisms to facilitate scientific collaboration.
    You can share access with others on a temporary, ad-hoc basis.  You
    might also be a manager of groups ("sub VOs") within Helmholtz AAI and
    can invite other people to collaborate on a longer-term basis.  In
    either case you are responsible for the behaviour of the people to
    whom you grant access.  They, too, must abide by these rules!  You may
    wish to refer to this document when explaining this.

5. You must protect your credentials

    In order to use the service, you may need to prove who you are by
    presenting credentials (passwords, private keys, tokens, ...).
    Depending on how you authenticate, you may need to present your
    credentials to the service itself or to some authentication service.
    You MUST NOT share these credentials with any other person.  We will
    NEVER ask you for these credentials in any correspondence.

6. Report any known or suspected security breaches

    Please inform the security contact below if you know or suspect the
    security of the service has been compromised; for example, you believe
    your password has been compromised or you see some activity that
    suggests you account is no longer secure.

7. Do not rely in the service beyond what is agreed

    The service is provided with our "best effort".  We try hard to keep
    the service running at all times and to avoid any loss of stored data;
    however, we make no promises.  Any data you store in this service is
    at your own risk.

8. Restricting or suspending your access

    We may need to restrict or suspend your use of the service for
    administrative, operational, or security reasons.  Where possible, we
    will give you advanced notice of any restriction or suspension;
    however, this is not always possible.  Therefore, we may unilaterally
    invoke the change, without any prior notice.

9.  Personal data

    You accept that we will process your personal data in accordance with
    our privacy policy.

10. Degrading the service

    You share the service with other scientists.  Do not do anything you
    think might disrupt the service the other users experience.  If we
    detect you are doing something that is causing a problem, we will take
    steps to correct that problem.

11. Please report problems you encounter.

    We try to provide a reasonable service.  However, if you experience
    some limitation or problem with a service, please let us know.

12. Breaking these rules.

    If you violate these rules then you may be liable for the
    consequences.  This may include your account being suspended and a
    report being made to your home organisation or to law enforcement.

---

## Contact points

-   The security contact is: d4@desy.de
-   Privacy policy is:
    English: <https://www.desy.de/data_privacy_policy/index_eng.html>
    Deutsch: <https://www.desy.de/datenschutzerklaerung/index_ger.html>
