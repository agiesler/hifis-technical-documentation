# dCache InfiniteSpace

The DESY-hosted InfiniteSpace service, based on the dCache system, allows Helmholtz scientists and their collaboration partners to store scientific data and have access to it through different protocols across all Helmholtz Centres and associated institutes. It was one of the first services connected to the Helmholtz AAI. It is provided as part of the [Helmholtz Cloud](https://helmholtz.cloud).

See [dcache.org](https://dcache.org) and the [dCache GitHub project](https://github.com/dCache) for more details on the project.

When using this service, you agree to comply to the service's [**Acceptable Use Policy**](AUP.md).

## Log in via Helmholtz AAI

Like for all Helmholtz Cloud, the login is performed via the Helmholtz AAI that allows you to use your home credentials to access all services.

!!! info "Usage for registered groups only."
    Usage of the service is not possible (and not reasonable) for single users.
    To get full access to this service, **you must belong to a Helmholtz-led collaboration group that has been granted access to the service**.

    - If you are head of a group/VO that wants to use dCache, follow the [application procedure](VO_application.md).
    - If you are a user and you think you belong to a group that should have access, please contact your group leader(s) in the first place, and [us](mailto:support@hifis.net?subject=[hifis-storage]) if everything else fails.

!!! info "This service requires some expert knowledge within your group / VO."
    This service is intended for large scale data handling.
    It provides a web front-end for _very_ basic operations, however all reasonable usage scenarios require some knowledge on linux command-line operations.
    HIFIS can provide limited support in setting up your storage environment -- however for sustainable operation, you need to have people with such basic knowledge in your group.
    When contacting our support, we might advise you to refer to more convenient storage solutions, like [Sync&Share](https://helmholtz.cloud/services/?filterSearchInput=Sync%2526Share).  
    The dCache InfiniteSpace scales horizontally avoiding the bottleneck of a single point of entry. Clients accessing data are always redirected directly to a data server. This comes with the caveat that you and your partner sites have to allow incoming and outgoing connections on higher ports to different nodes. The regular HTTPS port is only used for the browser frontend, all other operations happen with different nodes on different ports. This might cause problems with some of your partner sites depending on their firewall configurations.


As a user, to log in to the web frontend, you need to:

- Go to the [dCache InfiniteSpace](https://hifis-storage.desy.de/),
- select "Log in via OpenID-Connect Account",
- select "Helmholtz AAI(Keycloak)",
- select your home institution (or Google, Github, ORCID),
- identify yourself at your home institution.

See also our [illustrated tutorial on how such AAI login works in general](https://hifis.net/aai/howto).

## Getting and Managing Access

- See [here on how to apply for storage usage as a VO/group manager](VO_application.md)
- Ways to Access and Manage:
    - [Web frontend](https://hifis-storage.desy.de)
    - [API overview](https://hifis-storage.desy.de/api/v1/) (Swagger UI)
    - Using `rclone` to access the storage endpoint
    - Using the `gfal` tool kit
    - _others_

## Usage Scenarios

- Direct storage and publication using the web frontend, rclone etc.
- Streaming of data through WAN using suitable tools and protocols
- Read-Only access to DESY compute ressources if applicable
- Large scale data transfer with HIFIS: [High-level intro to HTS](https://hifis.net/use-case/2021/10/20/use-case-hts)
- Automation of distributed data processing pipelines: See [demonstration on Helmholtz Imaging use case](https://hifis.net/use-case/2023/04/18/service-orchestration). As always: [Contact us!](mailto:support@hifis.net)
- _More to come._

## Have fun!

If anything breaks, contact us at [support@hifis.net](mailto:support@hifis.net?subject=[hifis-storage]).
