# Apply for usage

Usage of the DESY-based dCache InfiniteSpace service is not possible (and not reasonable) for single users.
As a responsible manager of a group, managed as [Virtual Organisation](../../helmholtz-aai/howto-vos.md) (VO or sub-VO), you can apply for usage as follows:

## 1. Log in once to the central DESY infrastructure proxy (Keycloak)

Log in once so that your log in data is known and can be mapped correctly in subsequent steps:

- Log in at [DESY keycloak](https://keycloak.desy.de/auth/realms/production/account/#/), by clicking on "Helmholtz AAI" and selecting your institution.
- See also [pictured tutorial](https://hifis.net/aai/howto) on this procedure.

## 2. Send a mail to [support@hifis.net](mailto:support@hifis.net?subject=[hifis-storage])

Send a mail in English or German, stating:

- If you have **not** already [registered a Virtual Organisation (VO)](../../helmholtz-aai/howto-vos.md):
    - Brief purpose of your project (1-2 Sentences)
    - Who are the responsible contact person(s) - at least 1, better 2
    - (Short) Name of your Group, as it will appear in folder structures and [VO naming](../../helmholtz-aai/list-of-vos.md) schemes
    - Acceptable use policy, [template can be found here](../../helmholtz-aai/policies/helmholtz-aai/04_HIFIS-AUP.odt)
- If you have a VO already:
    - Please provide the exact name of your VO or Sub-VO that you want to give access to storage data.
- Acknowledgement of the [Acceptable Use Policy](AUP.md) of the Service
- Some usage details that allow us to assist you in setting up the best possible solution:
    - Users from which Helmholtz Centres or other institutions shall have full access to the data
    - Approximate amount of storage needed
    - Approximate time frame of storage needed
    - Any specific requirements on backup, availability, data transfer, use cases
- Preferred [Access Management Model](access_models.md):
    - [Simple](access_models.md#simple-model) Model (_default_), or
    - [Self-managed Access](access_models.md#self-managed-access-model) Model

## 3. We will come back to you with further instructions
