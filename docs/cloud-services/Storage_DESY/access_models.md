# HIFIS Storage Access Models

Read and/or write Access to the data in HIFIS Storage (dCache) can be fine-tuned.
However, fine-granular settings require a minimum of technical knowledge of at least one responsible designated person within the using group(s).
Any detailed management of the access rights must be performed by the user group(s) and possibly internal delegates.

!!! warning "Be aware:"
    The responsible persons of the user group(s) / VO(s) need to take care of the correct usage of the storage,
    including the corrrect access management.
    HIFIS can only provide _very_ limited assistance, and it is not possible to take over any management centrally.

When [applying for access](VO_application.md#apply-for-usage), the _VO manager(s)_ can choose one of two major models &mdash; and change later, if wanted:

The _[**Simple**](#simple-model)_ Model or the _[**Self-managed Access**](#self-managed-access-model)_ Model.

## Simple Model

This is the default, if not explicitly requested otherwise.

The folder assigned to the requesting VO can be:

- read from and written to by _all members_ of the respective VO _and all sub-VOs_.
- No data is accessible to the public.

Technically,

- the top level folder is set to
    - `uid=0` (root)
    - `gid=` group id of the requesting (Sub-)VO
    - access rights: `rwxrwx---`=`770`
- All ownerships and rights are inherited to sub-folders and files.
- A specific branch can be set open to public (e.g., `o+rx`) upon [request](mailto:support@hifis.net?subject=[hifis-storage]).

## Self-managed Access Model

When choosing this model, the applying _(Sub-)VO manager_ must designate one person that is capable of managing the access rights on the highest level, the _Access Manager_.

The _Access Manager_:

- can be the applying _(Sub-)VO manager_ or any other person within the (Sub-)VO,
- must be familiar with basic [POSIX / UNIX access rights](https://en.wikipedia.org/wiki/Unix_file_types#Representations) and their management,
- should log in beforehand once to [DESY keycloak](https://keycloak.desy.de/auth/realms/production/account/#/), by clicking on "Helmholtz AAI" and selecting her/his institution, to make the `uid` known.

When given the respective rights, the _Access Manager_:

- is responsible for managing the access rights on the highest level of the folder structure assigned to the (Sub-)VO,
- can modify rights and ownerships of specific folders and files, by
    - using the [dcache API](https://hifis-storage.desy.de/api/v1/),
    - until more readily usable handles are available: asking our [support](mailto:support@hifis.net?subject=[hifis-storage]).
- This includes:
    - creating subfolders and assign them (or any file) to (new) Sub-VOs,
    - delegating the _Access Manager_ right to other users - denoted as _Sub Access Managers_ for sub-folders,
    - changing access rights for the group (e.g., grant write permission, i.e., `g+w`), or the public (e.g., `o+rx`).

Technically,

- the top level folder is set to
    - `uid=` user id of the top level _Access Manager_
    - `gid=` group id of the respective (Sub-)VO
    - access rights: `rwxr-x---`=`750`
- All ownerships and rights are inherited to sub-folders and files.
