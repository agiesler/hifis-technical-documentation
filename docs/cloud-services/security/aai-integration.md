# Cloud Services - Security

The following brief documentation represent considerations for IT security related to the integration of the Helmholtz AAI.

!!! warning
    * This documentation is for convenience purposes only. It does not claim to be correct or complete in any way.
    * It is strongly advised that you **subscribe to a security alerting outlet**, e.g. [opencve.io](https://opencve.io) to stay updated on security issues related to your services.

Resources:

* [OAuth 2.0 authentication vulnerabilities: Leaking authorization codes and access tokens.](https://portswigger.net/web-security/oauth#leaking-authorization-codes-and-access-tokens)
* [What is going on with OAuth 2.0? And why you should not use it for authentication.](https://medium.com/securing/what-is-going-on-with-oauth-2-0-and-why-you-should-not-use-it-for-authentication-5f47597b2611)
* [Single Sign-On Security: Security Analysis of real-life OpenID Connect Implementations.](https://www.nds.ruhr-uni-bochum.de/media/nds/arbeiten/2020/11/30/Masterarbeit_Lauritz_Holtmann_Single_Sign-On_Security.pdf) L. Holtmann, Master thesis, RU Bochum
* [OpenCVE -- Open Source Security Alerting Platform.](https://opencve.io)

## Definitions and Abbreviations

* Authentication (AuthN): Assurance by an identity provider about a user's identity (Who are you?)
* Authorisation (AuthZ): A list of permissions/entitlements a user is granted (What are you allowed to do?), often in form of membership in a virtual organisation that is then mapped to a permission at a service provider
* OAuth2.0: A token-based authorisation protocol that carries information about users' entitlements
* OpenIDConnect (OIDC): an authentication layer on top of OAuth2.0
* Identity provider (IdP): an entity that provides users' identities and, optionally, assurance towards the identities
* OpenIDConnect-Provider (OP): an entity that provides OIDC/OAuth tokens for AuthN/Z, can be an IdP or a proxy on behalf of an IdP
* Service provider (SP), an entity that provides a service or resources, that can be accessed by an authenticated user with the correct authorisation
* Cross-Site-Request-Forgery (CSRF, XSRF): an attack which makes use of the statelessness of the HTTP protocol by using malicious requests that cause the response to a legitimate seeming request to be sent to a malicious URL
* JSON web token (JWT): a format for OAuth/OIDC tokens that specifies asymmetrically signed tokens

## General considerations for OAuth2.0 and OIDC

* The implicit authorization flow must be disallowed as it is deprecated and unsafe
* Wherever possible, the code authorization flow should be used together with
    * an unguessable state parameter
    * a nonce
    * or an http code challenge
    →  in order to prevent malicious third parties from stealing credentials through CSRF attacks
* The `redirect_uri` used between OP and SP in the Authorization code flow and token exchange must be verified against registered URIs (when setting up an OIDC client at an OP) and must not be pure wildcards (`https://example.com/*` is allowed)

## Considerations for OPs

* Anonymous dynamic client registrations against an OP should only be allowd from trusted hosts (i.e. bastion hosts of the home network), such that only users of the home organization can register clients
* Dynamic client registrations from anywhere should only be allowed for authenticated users
* Additionally, newly registered clients should need admin approval
* If possible, disallow `request_uri_parameter_supported` or at least enforce authentication for its use
* For JWT signatures, the signature algorithm must not be none but rather a safe algorithm provided by the implementation
