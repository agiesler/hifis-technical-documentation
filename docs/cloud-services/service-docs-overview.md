# Technical documentation on some services

For a subset of services, please find additional technical and detailed information:

* [bwSync&Share](bwSyncandShare_KIT/README.md) (provided by KIT)
* [Storage](Storage_DESY/README.md) (provided by DESY) (provided by DESY, prototype)
* [Gitlab](Gitlab_HZDR/README.md) (provided by HZDR)
* [LimeSurvey](LimeSurvey_DKFZ/docs/index.md) (provided by DKFZ)
* [Nubes](Nubes_HZB/README.md) (provided by HZB)
* [Rancher Managed Kubernetes](Rancher_DESY/README.md) (provided by DESY)

More details on services will be published when available.
