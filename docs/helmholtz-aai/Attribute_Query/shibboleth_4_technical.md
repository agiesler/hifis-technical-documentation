# Attribute Query: Specifics for Shibboleth 4

This is a collection of configuration files technically needed for Attribute Query response in Shibboleth 4.

For general Shibboleth configuration, see also [DFN Documentation :fontawesome-solid-up-right-from-square:](https://doku.tid.dfn.de/de:shibidp:config-deprovisionierung).

## Details

!!!warning
    This information is preliminary.
    The documentation will be completed as soon as possible.

* Exemplary configuration files are deposited below.
* `schacUserStatus.properties` must be located in `conf/attributes/custom/`.
* Implementation of `schacUserStatus` in `attribute-resolver.xml` is based on Active Directory as LDAP-Backend and uses the [UserAccountControl attribute](https://docs.microsoft.com/en-us/troubleshoot/windows-server/identity/useraccountcontrol-manipulate-account-properties).
  This needs to be handled differently when using OpenLDAP!
  (See example `attribute-resolver.xml` in DFN Documentation.)
* There is an example for a CERN Login in `relying-party.xml`, maybe it helps for understanding.

## Configuration Files

* [`attribute-filter.xml`](./attribute-filter.xml)
* [`attribute-resolver.xml`](./attribute-resolver.xml)
* [`relying-party.xml`](./relying-party.xml)
* [`schacUserStatus.properties`](./schacUserStatus.properties)

## Questions?

If you have further questions, please contact <support@hifis.net>.