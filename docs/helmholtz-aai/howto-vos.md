# Registration of a Virtual Organisation (VO) 

In order to request and
register a new VO,
**you need to fulfil some conditions**:

- You must be an employee of a Helmholtz institution.
- You must comply with the [AAI policies](policies/README.md) and related duties of VO Management.

**Send an email to our [Helpdesk](mailto:support@hifis.net?subject=[VO request]),
providing the following information**:

* VO name
* VO description
* VO manager
* VO acceptable use policy. Templates can be found here: [odt](policies/helmholtz-aai/04_HIFIS-AUP.odt) / [pdf](policies/helmholtz-aai/04_HIFIS-AUP.pdf).
* _Optional:_ VO privacy policy. Templates can be found here: [odt](policies/helmholtz-aai/05_HIFIS-PP.odt) / [pdf](policies/helmholtz-aai/05_HIFIS-PP.pdf).
* _Optional:_ Mail adress where users can apply for membership in your VO.

We will get in touch with you, while setting up the VO or if something is missing.

!!! info "What is this about? Why do I need a VO?"
    - VO are technical representations of (research) groups and are the key element of authorisation in the Helmholtz Cloud.
    - Some services don't need VOs, but most do, in order to control access granted to users and groups.
      See also [our explanation of the AAI concepts](../concepts).
    - Don't hesitate to [ask us via our Helpdesk](mailto:support@hifis.net), should you have any further questions.

!!! warning "MFA enforced"
    Note that, from July 2023, we enforce multi-factor authentication (MFA) in order to manage groups / VOs. [See here on how to enable this for you](howto-mfa.md).

## Preconditions

You (as a VO admin) need to be able to authenticate with the [assurance](assurance.md) of RAF Cappuccino, i.e. you need to identify with your passport at your Identity Provider.

### Responsibility

As an administrator of a Virtual Organisation you take a substantial share of
responsibilities for a working process. The requirements come from the Services.
Many services have requirements on the quality of the user identity
[assurance](assurance.md) and on the general quality of the identity provider. 

Depending on the service (in this case those allow shell access or data storage)
this often requires the users to have shown a passport at their home-IdP and
also require the home-IdP to support certain security procedures.

### International Users

In Helmholtz AAI we want to enable users for which those criteria often aren't met.
Therefore, we offer the possibility to add all kinds of users to a VO, but we
require the VO admin to guarantee that an appropriate level of identity vetting
has taken place.
<!-- We are currently in the process of deriving a comprehensive
set of guidelines to help VO admins to do their job. -->

------------
### Further information about Policies

As defined in the top level policy, VO administrators have several tasks to
fulfil:

- Abide by the following policies:
    - [Helmholtz AAI Top Level Policy](policies/helmholtz-aai/00_HIFIS-Top-Level-Policy.pdf)
    - [Membership Management Policy](policies/helmholtz-aai/01_HIFIS-VOMMP.pdf)
    - [Security Incident Response Procedure](policies/helmholtz-aai/02_HIFIS-SIRP.pdf)
    - [Policy on the Processing of Personal Data](policies/helmholtz-aai/03_HIFIS-PPPD.pdf)

- If necessary, define AUP and PP policies for your VO by extending the following templates:
    - [Acceptable Use Policy Template (AUP-Template)](policies/helmholtz-aai/04_HIFIS-AUP.odt) / [PDF version](policies/helmholtz-aai/04_HIFIS-AUP.pdf)
    - [Privacy Policy Template (PP-Template)](policies/helmholtz-aai/05_HIFIS-PP.odt) / [PDF version](policies/helmholtz-aai/05_HIFIS-PP.pdf)

In most cases, a PP is not necessary if the VO is managed at Unity and you do not additionally process any personal data.

