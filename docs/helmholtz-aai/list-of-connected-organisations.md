# List of Connected Organisations
The Helmholtz AAI is connected to a huge set of organisations which allows authentication, using the organisational accounts.

## eduGAIN
The Helmholtz AAI participates in eduGAIN, which offers user authentication with the organisational account across the whole world. The organisation needs to participate in eduGAIN, too. At the moment, more than 4000 organisations participate in eduGAIN.
You can check if your organisation is part of eduGAIN, [here](https://technical.edugain.org/entities) and setting the Entity Type to Identity Providers.

!!! Info "Potential problems"
    Sadly there might be problems during the login, even if your organisation is participating eduGAIN.
    
    1. Some national federations, e.g. the Dutch one, are operating an opt-in policy.
       Here each service, where you want to login, needs to be accepted by your organisation at the first time.
       You will receive an error on your organisation site with a message similar to "Error: Helmholtz AAI not accessible through your institute."  
       **Please follow the instructions on that page and contact your local IT support.**
    
    2. In some cases the organisation does not release all mandatory attributes to the Helmholtz AAI.
       In this case you will get an error on the Helmholtz AAI site about "The remote authentication was successful, however the server's policy requires more information then was provided to register your account."
       In this case your organisation needs to release more information.  
       **Please contact either:**
           - your local IT support and request to release all mandatory data or 
           - [us](mailto:support@hifis.net?subject=[aai]) and we will submit the request on behalf of you.

## Helmholtz Centres
The Helmholtz AAI focuses but do not limit to Helmholtz centres. In opposition to other organisations, some further information about the connection to Helmholtz centres are gathered here.

Additionally, it is listed if the centres can be found / filtered by their shortname.

| Center  | IdP present  | in DFN AAI | User(s) | shortname provided |
|:-------:|:----:|:--------:|:----:|:---:|
| AWI     | yes  | advanced | yes | yes |
| CISPA   | yes  | basic    | yes | yes |
| DESY    | yes  | advanced | yes | yes |
| DKFZ    | yes  | advanced | yes | yes |
| DLR     | yes  | advanced | yes | yes |
| DZNE    | yes  | advanced | yes | yes |
| FZJ     | yes  | advanced | yes | yes |
| Head Office | yes | advanced | yes  | no  |
| GEOMAR  | yes  | advanced | yes | yes |
| GFZ     | yes  | basic    | yes | yes |
| GSI     | yes  | basic    | yes | yes |
| hereon  | yes  | basic    | yes | yes |
| HMGU    | yes  | advanced | yes | yes |
| HZB     | yes  | advanced | yes | yes |
| HZDR    | yes  | advanced | yes | yes |
| HZI     | yes  | advanced | yes | yes |
| KIT     | yes  | advanced | yes | yes |
| MDC     | yes  | advanced | yes | no  |
| UFZ     | yes  | advanced | yes | yes |

## Social identity providers
Beside the organisational accounts the Helmholtz AAI supports some social identity providers (IdPs). Those IdPs can be used by users, whose organisation does not participate in eduGAIN. Accounts, authenticated with social IdPs, may have a lower assurance then organisation based accounts. A lower assurance may lower the set of available services or lower the functionality on services. The available social IdPs are:

* GitHub
* Google
* ORCID
