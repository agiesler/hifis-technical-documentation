# Mailinglists for Helmholtz AAI

These lists are used in the Helmholtz AAI:

##### Public

- <support@hifis.net>:  
  This is the general contact and support address, please use that preferably.

- <ds-support@fz-juelich.de>:  
  The technical support of the service provider of the Helmholtz AAI central instance, Unity at FZJ.

- <security@hifis.net>:  
  This is the Infrastructure Security Contact point as described [here](security-response.md).

---

##### Moderated

- <aai@hifis.net>:  
  This is the contact list of the [HIFIS working group on AAI](https://hifis.net/services/#backbone).
  It is open for anybody to post, but moderated.
