## Helmholtz AAI Policies

All Policies here are in line with the [**AARC policy development kit**.](https://aarc-project.eu/policies/policy-development-kit)

The Helmholtz AAI Policies consist of the documents listed below. 

* [**TLP** -- Top Level Infrastructure Policy](helmholtz-aai/00_HIFIS-Top-Level-Policy.pdf)
* [**PPPD** -- Policy on the Processing of Personal Data](helmholtz-aai/03_HIFIS-PPPD.pdf)
* [**SIRP** -- Security Incident Response Procedure](helmholtz-aai/02_HIFIS-SIRP.pdf)
* [**VOMMP** -- Virtual Organization (VO) Membership Management Policy](helmholtz-aai/01_HIFIS-VOMMP.pdf)
* [**VOLCM** -- Virtual Organisation (VO) Life Cycle Management](helmholtz-aai/91_VO-lifecycle.pdf)

All documents are also available as a [concatenated pdf](helmholtz-aai/Helmholtz-AAI-policies.pdf).

Templates:

* [**PP** -- Privacy Policy Template](helmholtz-aai/05_HIFIS-PP.odt) / [PDF version](helmholtz-aai/05_HIFIS-PP.pdf)
* [**AUP** -- Acceptable Use Policy Template](helmholtz-aai/04_HIFIS-AUP.odt) / [PDF version](helmholtz-aai/04_HIFIS-AUP.pdf)


#### Overview

The following graph gives an overview of the Helmholtz AAI policies, specifying, for each policy, the *Participant* that defines (or should define) the policy, and the *Participant*(s) that must abide by the policy.

![Helmholtz AAI Policy Overview](helmholtz-aai/Policy_overview.png)

#### Checklist

Please also have a look at the [**Checklist**](helmholtz-aai/90_checklists.pdf) for an overview on relevant procedures for you.

---

## Policy Frameworks
The policy frameworks are not policies themselves, they introduce further
necessary concepts.  The policy frameworks provide a conceptual structure
within which actual policies are defined.

- SNCTFI - Scalable Negotiator for a Community Trust Framework in Federated Infrastructures
    - [https://aarc-project.eu/policies/snctfi/](https://aarc-project.eu/policies/snctfi/)
    - [https://aarc-project.eu/wp-content/uploads/2017/07/Snctfi-v1.0.pdf](https://aarc-project.eu/wp-content/uploads/2017/07/Snctfi-v1.0.pdf)

- SIRTFI (policy framework) - Security Incident Response Trust Framework for Federated Identity
    - [https://aarc-project.eu/policies/sirtfi/](https://aarc-project.eu/policies/sirtfi/)
    - [https://aarc-project.eu/wp-content/uploads/2017/06/Sirtfi-1.0.pdf](https://aarc-project.eu/wp-content/uploads/2017/06/Sirtfi-1.0.pdf)

- GÉANT Data Protection Code of Conduct (DPCoCo)    
    - [https://wiki.refeds.org/display/CODE/Data+Protection+Code+of+Conduct+Home](https://wiki.refeds.org/display/CODE/Data+Protection+Code+of+Conduct+Home)

- Research and Scholarship Entity Category
    - [https://refeds.org/category/research-and-scholarship](https://refeds.org/category/research-and-scholarship)

