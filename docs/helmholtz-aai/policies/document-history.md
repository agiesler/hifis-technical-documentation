# Helmholtz AAI Policies Versions

| Version | Date | Status | Changes | Affected Parts | Links |
| ------ | ------ | ------ | ------ | ------ | ------ |
| 1.0 | November 2020 | Published | Initial Document | All | [Full PDF](helmholtz-aai/Helmholtz-AAI-policies.pdf)<br>[Final Merge](https://codebase.helmholtz.cloud/hifis/overall/hifis-technical-documentation/-/merge_requests/92) |
