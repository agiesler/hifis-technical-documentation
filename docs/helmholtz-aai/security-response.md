# Security Incident Response

Following our [**Security Incident Response Procedure**](policies/helmholtz-aai/02_HIFIS-SIRP.pdf),
which is part of the [Helmholtz AAI Policies](policies/README.md),
Helmholtz AAI operates a **central mailing list** for incident response:

- <security@hifis.net>: This is the
  Infrastructure Security Contact point, which is open for anybody to post.

Subscribers to this list are e.g.:

- Contacts for the SP-IdP-Proxy (Unity @ FZJ)
- HIFIS Cluster Contacts

Optional:

- Service's site-security contacts (i.e. CERTs of participating sites)

In case of an incident, this Infrastructure Security Contact point will
appoint and collaborate with the relevant Security Incident Response Coordinator(s)
for the given incident.

# SIRTFI

The Helmholtz AAI uses [SIRTFI](https://refeds.org/sirtfi), the Security Incident Response Trust Framework for Federated Identity, to communicate and collaborate with other organisations and instances, which are involved in the security incident.
