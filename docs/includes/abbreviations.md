*[2FA]: Two-factor Authentication
*[AARC]: Authentication and Authorisation for Research and Collaboration initiative
*[AAI]: Authentication and Authorisation Infrastructure
*[API]: Application programming interface
*[CI/CD]: Continuous Integration and Deployment
*[CI]: Continuous Integration
*[CD]: Continuous Deployment
*[Codebase]: Helmholtz Codebase: Helmholtz-wide Gitlab instance, hosted at HZDR
*[DFG]: Deutsche Forschungsgemeinschaft
*[DFN]: Deutsches Forschungsnetz
*[EGI]: European GRID Infrastructure
*[EOSC]: European Open Science Cloud
*[FTS]: File Transfer System
*[FTS3]: File Transfer System
*[HIFIS]: Helmholtz Federated IT services
*[HPC]: High Performace Computing
*[HTC]: High Throughput Computing
*[IdP]: Identity Provider
*[IdPs]: Identity Providers
*[ITIL]: Information Technology Infrastructure Library
*[JSON]: JavaScript Object Notation
*[KPI]: Key Performance Indicator
*[LHC]: Large Hadron Collider
*[MFA]: Multi-Factor Authentication
*[NFDI]: Nationale Deutsche Forschungsdateninfrastruktur
*[OIDC]: OpenID Connect
*[OTP]: One-time password
*[Plony]: Helmholtz Cloud Service Database, based on Plone, hosted at HZB
*[REST]: Representational state transfer
*[SAML]: Security Assertion Markup Language
*[SIRTFI]: Security Incident Response Trust Framework for Federated Identity
*[SSO]: Single-sign on
*[Unity]: Unity IdM, the software stack currently used for Helmholtz AAI community proxy, hosted at FZJ
*[VO]: Virtual Organisation (representation of collaboration group in AAI)
*[VOs]: Virtual Organisations (representation of collaboration groups in AAI)
*[WLCG]: Worldwide LHC (Large Hadron Collider) Computing Grid
