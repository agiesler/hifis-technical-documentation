# Policies + Guidelines

The Policies and Guidelines related to Helmholtz AAI and Helmholtz Cloud, as worked out by HIFIS, are documented here.

- [Helmholtz AAI Policies](helmholtz-aai/policies/README.md)
- [Process Framework for Helmholtz Cloud Service Portfolio](process-framework/Chapter-Overview.md)
