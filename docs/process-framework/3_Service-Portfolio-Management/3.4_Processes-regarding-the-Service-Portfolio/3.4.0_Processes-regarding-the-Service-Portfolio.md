# Processes regarding the Service Portfolio

This Process Framework includes descriptions of all processes occuring during a service's lifecycle.

The Processes already visualized, explained and described in this document are:

- the [Service Recruiting process](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.1_Service Recruiting Process.md)
- the [Service Onboarding process for new services](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services.md)
- the [Follow up Onboarding process](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.3_Follow Up Onboarding Process.md)
- the [Operation of services](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.4_Operation-of-Services.md)
- the [Service Offboarding process](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.5_Offboarding-Process.md)
- The [Service Portfolio Review process](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.6_Review-Process-for-the-Service-Portfolio.md)

For [Service Operation](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.4_Operation-of-Services.md) we defined which service information can be edited by whom. There is no further process description for Service Operation since it is not a process in that sense but rather a ruleset of what can be edited by whom.

The processes described in this Process Framework occur at the following stages of a service's lifecycle from Service Portfolio view:
![Alt-Text](../../0_Corresponding-files/HIFIS-Cloud-SP_Service-Portfolio-Components_and_Processes-Overview2.jpg)

## Roles involved in the Service Portfolio processes

The following roles are involved in different combinations in the Service Portfolio processes:

| Role | Description | Belonging to | 
| ------ | ------ | ------ | 
| Service provider | Group/department of a Helmholtz centre providing (or owing to provide) a service in Helmholtz Cloud | Helmholtz centre | 
| Using centre | Group/department of a Helmholtz centre or whole centre being allowed to use (or owing to use) a service provided by a service provider in Helmholtz Cloud | Helmholtz centre | 
| User within using centre | Individual within the using centre who actually uses a service provided by a service provider in Helmholtz Cloud | Helmholtz centre | 
| Requester (user/user group) | Individual or group communicating the need for a service in Helmholtz Cloud which is not yet provided | Helmholtz centre | 
| Service Portfolio Manager | Responsible for Service Portfolio Management of Helmholtz Cloud | HIFIS | 
| Service Integration Manager | Responsible for technical integration of services into Helmholtz Cloud | HIFIS | 
| Cloud Portal Manager | Responsible for construction and maintenance of Helmholtz Cloud Portal | HIFIS | 
| Cloud Platform Manager | Responsible for coordination of Helmholtz Cloud activities and roles named above | HIFIS | 
| HIFIS  Manager | Responsible for coordination of HIFIS activities (all Clusters) | HIFIS | 
| KPI Coordinator | Responsible for coordination of KPI measurements for Helmholtz Cloud services | HIFIS | 
