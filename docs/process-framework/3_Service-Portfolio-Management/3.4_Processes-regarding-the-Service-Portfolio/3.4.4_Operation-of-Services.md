# Operation of services

When a service is operated in Helmholtz Cloud, it might occur over time that the service information in the Service Catalogue entry become outdated and therefore needs to be updated.

The update of service information is not a process in that sense, but of course it is necessary to define which service information can be edited by whom. This definition can be found [here](../../0_Corresponding-files/HIFIS-Cloud-SP_Service-Canvas-Rights-Matrix-for-operation-v1.7.2.pdf)

Generally, every member of the Service Operation Group (group of people defined by service provider per service) can suggest changes regarding Service Information in Plony. In order to get service information changed, the suggested changes are sent to the responsible Service Manager (defined in Service Canvas of the corresponding service). The Service Manager may adapt the suggested changes or simply sends the suggested changes as a change request to HIFIS.

On HIFIS side, the Service Portfolio Manager receives all change requests. For all fields transferred to Helmholtz Cloud Portal, the Service Portfolio Manager needs to discuss the requested changes with the HIFIS Team (Service Integration Manager, Cloud Portal Manager (CP), Architecture Manager (AC), Cloud Platform Manager (PM)). This is also true for fields that may have implications on service provision, e.g. Connection with HIFIS Helpdesk and Availability for external users (for details see [full rights matrix](../../0_Corresponding-files/HIFIS-Cloud-SP_Service-Canvas-Rights-Matrix-for-operation-v1.7.2.pdf)). Change requests concerning other fields can be accepted by the Service Portfolio Manager without approval from the HIFIS team. Additionally, the Service Integration Manager can suggest changes and send them to the Service Portfolio Manager for approval.

Please keep in mind that in case of a service upgrade (larger functional extension of a service), the [Service Portfolio Review process](3.4.6_Review-Process-for-the-Service-Portfolio.md) is triggered.
