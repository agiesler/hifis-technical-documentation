# Offboarding process for Helmholtz Cloud Services

There are two Offboarding types in HIFIS context: <br>
- Complete service Offboarding (1) <br>
- Offboarding of a using centre (2) <br>

Whereas (2) is a subset of process steps described in (1).

(1) **Complete service Offboarding** means that the service should not be offered in Helmholtz Cloud anymore and therefore will be completely removed. Consequently, no centre can use the service anymore, it is removed from Helmholtz Cloud Portal and switches its status in portfolio to "Retired services". Complete service Offboarding may be initiated by the last using centre discontuing service usage.

(2) **Offboarding of a using centre** means that a single using centre discontinues service usage but the service is still offered to other using centres via Helmholtz Cloud Portal. The service will not be removed from Helmholtz Cloud Portal and remains in the Service Catalogue, but cannot be used by the former using centre anymore. This might occur simply due to the fact that the service is not required anymore from using centre's side or due to legal framework breaches.

## Complete service Offboarding

It might come up due to a review that a service is not used anymore, does not fulfill Exclusion criteria anymore or should be offboarded for other reasons. The primary goal of this process is to remove services from the Service Catalogue in a transparent and structured way. Therefore, a [Retirement Form](../../0_Corresponding-files/HIFIS-Cloud-SP_Retirement-form-content_v4.pdf) was worked out to capture all important information for Offboarding at the very beginning of the process.

The roles involved in the Offboarding process are: the service provider (Helmholtz Centre), the using centre (Helmholtz Centre), the Cloud Portal Manager (HIFIS) and the Service Portfolio Manager (HIFIS). The main tool supporting the Offboarding process will be Plony where the Retirement Form will be integrated.

The triggers for the Offboarding Process are:

- Service provision should be discontinued (no more using centres or other reasons) (triggered by service provider) (1)
- Service not required anymore/legal framework breach (triggered by using centre) (2)
- Service Portfolio Review triggered the retirement of the service (triggered by HIFIS) (1)

Whatever the trigger, the first thing that needs to be done by the service provider is to fill out the Retirement form. The roles that are authorized to fill out the Retirement Form are the Service Owner and the Centre Manager/Contact.
The **Retirement form** asks for the following information: 

| Field | Description/Explanation | Required value |
| ------ | ------ | ------ |
| Service name | List of services the service provider currently offers in Helmholtz Cloud to choose from | Yes |
| Offboarding type | Complete service Offboarding service or Offboarding of a using centre | Yes |
| Offboarding of a using centre | List of using centres the service is provided to | if "Offboarding type" is "Offboarding of a using centre" |
| Reason for Offboarding | Short explanation why Offboarding should be initiated | No |
| Contact person for Offboarding at service provider | Person HIFIS can get back to while performing the Offboarding process | Yes |
| Contact person for Offboarding at using centre | Person HIFIS can get back to while performing the Offboarding process | Yes |
| Dependencies to other services | Dependencies to other services | Yes |
| Deprovisioning | Deprovisioning state/step dates and which user groups are affected | Yes |
| Backup strategy | Verification of information given to HIFIS earlier regarding backup strategy are still up to date – to be sure how data of Retired service is backed-up/ treated | Yes |
| Planned Offboarding date | Date until which Offboarding should be completed | Yes |

As soon as the Retirement Form is sent to HIFIS, the responsible Centre Manager/Contact and IT head (if not the same person) receive a notification via mail. In parallel, or in the best case even before Offboarding is initiated, the service provider should inform the using centre(s) about the service being discontinued. This includes asking the users to store their data from the service as well as agreeing how and when backups from service provider side are handed over to using centre.

It might be the case that Offboarding is not voluntary from Using Centre side; therefore the Using Centre might require another service replacing the functionality of the offboarding service. The Service Portfolio Manager will ask whether the Using Centre requires support in finding an equivalent/ similar service and coordinate the transfer to the alternative service (by initiating the [follow up onboarding process](../../3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.3_Follow Up Onboarding Process.md)).

After having clarified a potential transfer to an alternative service, the using centre(s) will be officially informed by the Service Portfolio Manager that the service's status in Helmholtz Cloud Portal will switch to "Discontinued" soon. As soon as the status changed, using centres should stop using the service and ultimately save their data, if not done yet. The next step to offboard the service is to technically disable the VO's of the using centre(s), done by Cloud Portal Manager, which equals the rollback of the authorisation to use the service. In parallel, the Service Portfolio Manager needs to archive the corresponding SLA's to keep Contract Database up-to-date. As soon as all VO's are technically disabled, the Service Portfolio Manager will set the service's status in Plony to "Retired". When being in status “Retired”, the service is officially moved from Service Catalogue to Retired services.

In order to verify that all data has been deleted after a defined timeframe, checks are performed between service provider and using centre as well as between service provider and Cloud Portal Manager. The correct data deletion is documented in an acceptance protocol. Only after having verified data deletion, the service will be finally removed from Helmholtz Cloud Portal. This is the end point of the offboarding process.

For the whole [process visualization](../../0_Corresponding-files/HIFIS-Cloud-SP_Offboarding-Process_v4.pdf) and [step-by-step explanation](../../0_Corresponding-files/HIFIS-Cloud-SP_Explanation-of-each-process-step-of-Offboarding-process_v4.pdf) please check the corresponding files.

## Offboarding of a using centre

When a single using centre is offboarded but the service is still offered to other centres, the Offboarding process described above skips some process steps. The following points are not done when Offboarding a single using centre: <br>
- Service's status in Helmholtz Cloud Portal will not be set to "Discontinued" <br>
- Service's status in Plony will not be set to "Retired" <br>
- Service won't be removed from Helmholtz Cloud Portal <br>

The corresponding process steps are marked in green in the [process visualization](../../0_Corresponding-files/HIFIS-Cloud-SP_Offboarding-Process_v4.pdf).

The rest of the process is as described above.
