# Definition of Service Specific Roles via AAI: DataHub

For Helmholtz [DataHub](https://datahub.erde-und-umwelt.de/en/), the following role definitions are being implemented within Helmholtz AAI, as Modifications of "[Variant 1](README.md#variant-1-group-based-user-role-for-one-or-more-services)":

## Variant 1a: Group-based User Role, for Specific Service

To define a specific role of a user within a VO based group, specifically for a service:

- The service provider defines a naming scheme for a specific sub-group, following the convention
    - `<provider name>-<service name>-<role name>`.
    - For example: `<gfz-sms-admin>`, `<gfz-sms-member>`.
    - The definition shall be documented here for the specific service.
    - Service provider is responsible to inform the VO/group managers of all allowed VOs/groups on this definition and its consequences.
- VO administrator or VO subgroup manager creates a Sub-VO (group) within her specific group (VO), following this naming scheme.
- VO administrator or VO subgroup manager is responsible to define the members of these groups, according to VO membership policies.
- Service checks for respective subgroup memberships by checking for respective group claim.

## Variant 1b: Group-based User Role, use internal group management structures via Helmholtz AAI:

To define a specific role of a user, specified in the internal (institute) group structures (and internal groups are enabled to be released for external purposes to Helmholtz AAI), specifically for a service:

- The service provider defines a name scheme for such groups: `<group name>.<provider name>-<service name>-<role name>`
    - with `<group name>` identifying the organizational unit, e.g., structure/group/topic/section
    - and `<provider name>-<service name>-<role name>` as in variant 1 showing the provider and name of the service and also the corresponding role name
- As this group is handed via home IdP to Helmholtz AAI and via Helmholtz AAI to the service it has the following form:
  `<prefix>:<namespace>:<group_scheme>#authority`
- For example: `urn:geant:helmholtz.de:gfz:group:department5.gfz-sms-member#idp.gfz-potsdam.de`
    - with the prefix defined by Helmholtz AAI and a [registration](../../helmholtz-aai/namespaces-registry.md#urngeanthelmholtzde-registry) for the namespace to ensure unique group names. Therefore, the prefix + namespace `urn:geant:helmholtz.de:gfz:group` will only be used for internal GFZ groups. This also guarantees that groups with the same name from different institutions are separate groups.
    - The authority `#idp.gfz-potsdam.de` due to Helmholtz AAI/AARC convention.
	- These group memberships are released as values for `eduPersonEntitlement` by the IdP of the home organization of a user
	- Creation and management of groups is organized according to the internal infrastructure of the home institution
	- Service checks for respective group name and institution to identify the groups and respectively the roles of its members

## Variant 1c: Group-based User Role, for Multiple Services with a Shared Context
To define a specific role of a user that holds for multiple services with a shared context.

- Naming convention is shared for different service providers that agreed to it but do not have to use it exclusively.
    - Different services of the DataHub project share the same members: `<datahub-default-member>` according to naming convention
		- datahub: shared context as provider name/hosted in the DataHub context
		- default: general service name as several services are addressed at the same time (individual management still possible next to „shared“ management option)
		- member: role name – shared groups only make sense where the user groups are identical for different services. This holds in general only for members getting access to different services for a topic but does not hold for admins or other specific roles.
	- needs to be documented and shared with the VO/group managers of all allowed VOs/groups on this definition and its consequences
- Possible to use for VO based group management and internal group management; the latter with members within institution only
- All engaged services check for respective group name (VO)/ group name + institution to identify the groups and respectively the roles of its members.
