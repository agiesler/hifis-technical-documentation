---
title: "Automating dependency updates in the Helmholtz Codebase"
---

!!! warning "Public Beta"
    The service is rolled out as a public beta.
    At the moment we cannot guarantee that no breaking changes need to be implemented.
    Also the service can be turned off at any time.
    We are curiuos for your [feedback](https://hifis.net/contact.html).

# Automating dependency updates in the Helmholtz Codebase

The Helmholtz Codebase is configured in a way that it supports automatic
dependency updates using a tool called
[dependabot-gitlab](https://dependabot-gitlab.gitlab.io/dependabot/).
Therefore it makes use of
[dependabot-core](https://github.com/dependabot/dependabot-core)
which is known from GitHub.

The bot will automatically create Merge Requests for dependency updates
helping you to keep your dependencies up-to-date thereby enhancing the security
of your project.

## Supported Ecosystems

The list of supported ecosystems is given
[here](https://docs.github.com/en/code-security/dependabot/dependabot-version-updates/about-dependabot-version-updates#supported-repositories-and-ecosystems).
Among others, Python, Docker or Git submodules are supported.

## Configuration

In order to configure automatic dependency updates for your project a few steps
need to be accomplished.

1. Add the `hifis-bot` user as a member to your project.
   At least the role `Developer` needs to be assigned.
2. Add a file called `.gitlab/dependabot.yml` into the root of your repository.
   A minimal configuration for supporting the Python package managers `pip`,
   `poetry` or `pipenv` is given below.

   ```yaml
   version: 2
   updates:
     - package-ecosystem: "pip"
       directory: "/"
       schedule:
         interval: "daily"
       rebase-strategy: "auto"
   ```

   All configuration options are listed in the
   [documentation](https://dependabot-gitlab.gitlab.io/dependabot/config/configuration.html).

3. Within at most an hour the service will take note of the new project
   and will start to submit dependency updates according to the definitions in
   `dependabot.yml`.
   If you configure updates daily it can take at most 24 hours until the first
   merge requests will be submitted.

### Webhooks - Interacting with the Bot

In order to communicate with the bot via comments in your Merge Request a
[webhook](https://codebase.helmholtz.cloud/help/user/project/integrations/webhooks.html)
needs to be configured.
Therefore, please follow these steps:

1. In your project navigate to `Settings > Webhooks`.
2. Create a new webhook with these parameters, if not already present:

     * URL: `https://dependabot.hzdr.de/api/hooks`
     * Triggers:
         * `Push events` - default repository branch (typically `main` or `master`)
         * `Merge request events`
         * `Comments`
         * `Pipeline events`
     * Keep `Enable SSL verification` enabled.

3. Save the webhook by clicking `Add webhook`.

## Interacting with the Bot

Interacting with the bot is possible via Merge Request commands which are
entered as comments.
At the time of writing two commands can be issued.

### Rebase

Add the comment `@hifis-bot rebase`, which will perform a merge request rebase.
This command does essentially the same action as GitLab built in `/rebase` command.

### Recreate

The comment `@hifis-bot recreate`  will recreate the merge request
resolving all merge conflicts that might be present.

## Resources

* [Dependabot-GitLab Docs](https://dependabot-gitlab.gitlab.io/dependabot/guide/)
* [Dependabot Docs](https://docs.github.com/en/code-security/dependabot/dependabot-version-updates/about-dependabot-version-updates)
* [GitLab project](https://gitlab.com/dependabot-gitlab/dependabot)
