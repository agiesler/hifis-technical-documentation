---
title: Make OER in Helmholtz Cloudbase discoverable by OERSI
---

# How to make OER in Helmholtz Codebase discoverable by OERSI

## About OER

### What is OER

_Open Educational Ressources_ include all kind of media intended to transport knowledge available under an OER-compatible license like [_CC-BY_][license-cc-by].
This could be websites, presentations, video tutorials, course material, worksheets and many more.

### What is OERSI

The [_OER Serach Index_](https://oersi.org) is a platform that collects various OER offers from different platforms and makes them discoverable by listing them along with their metadata and related information on their page where learners can search for material matching their topic of interest.

OERSI queries various hosting platforms for sources marked as OER, such as university libraries, GitLab / GitHub and also (since January 2023) the [_Helmholtz Codebase_](https://codebase.helmholtz.cloud).

## Steps to make your OER discoverable

This assumes you have a repository on the _Helmholtz Codebase_ containing the OER you want to publish.

1. Create a YAML-file containing the required metadata. The OERSI provides a very helpful [online generator][oersi-metadata-generator] that can do this for you.
    * Note that the **URL** field is the URL that points to the final ressources to be shared, not the repository the material comes from.
2. Copy the generated `metadata.yml` into the root folder of your repository.
3. Go into the project settings and under _General_ add the topic `Open Educational Ressources`.
4. Make sure the projects' visibility is set to _public_.
5. When you want your work to be shared also ensure that in the `metadata.yml` the value for `creativeWorkStatus` is set to `Published`.

Now the magic happens on its own and OERSI will discover your work automatically within a day.
You can change / update your metadata at any time in case of changes.

### Example Project

In case you would like to see an example, you can take a look at our [OOP in Python course][course-oop-gitlab], especially the `metadata.yml` file.
Notice the settings of the topic as shown below:

![Topic set to include "Open Educational Ressources"](screenshots/settings-topic.png)

[license-cc-by]: https://creativecommons.org/licenses/by/4.0/
[oersi-metadata-generator]: https://oersi.gitlab.io/metadata-form/metadata-generator.html
[course-oop-gitlab]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-oop-in-python
