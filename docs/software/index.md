---
title: Service Documentation - HIFIS Software
---

# Service Documentation - HIFIS Software

HIFIS Software operates services that are available to members of the
Helmholtz Association and their guests.
On these pages we would like to answer and document service-specific questions.
This should be a useful addition to the general documentation of the services.

## Services

Currently, services marked with a green tick are available for use.
Services that are definitely planned to be made available in the future will
be added to the list with a grayed icon.

* [x] [Helmholtz Codebase aka GitLab](https://codebase.helmholtz.cloud) (part of [Helmholtz Cloud][cloud])
    * [x] Continuous Integration via GitLab CI
    * [x] Container and Package Registry
    * [x] Pages
    * [x] Indexing of Open Educational Ressources in [OERSI](https://oersi.org)
* [x] [Mattermost](https://mattermost.hzdr.de) (part of [Helmholtz Cloud][cloud])
    * [x] [CircleCI plugin](https://mattermost.com/marketplace/circleci/)
    * [x] [GitHub plugin](https://mattermost.com/marketplace/github-plugin/)
    * [x] [GitLab plugin](https://mattermost.com/marketplace/gitlab-plugin/)
    * [x] [Matterpoll plugin](https://mattermost.com/marketplace/matterpoll/)
    * [x] [Remind plugin](https://mattermost.com/marketplace/remind-plugin/)
    * [x] [WelcomeBot plugin](https://mattermost.com/marketplace/welcomebot-plugin/)
* [x] [Kroki](https://kroki.hzdr.de)
* [x] [HIFIS Helpdesk](https://support.hifis.net)
* [x] [Helmholtz AI Voucher Backend](https://zammad-voucher.helmholtz.ai/)

## Changelog

We document user-visible changes and improvements of the services on the
Changelog page.

* [GitLab](gitlab/changelog.md)
* [Mattermost Changelog](mattermost/changelog.md)

## Reproducible Service Administration via Ansible

All services are transparently deployed via so-called public
[Ansible roles](https://galaxy.ansible.com/hifis/)
and Helmholtz internal accessible
[Ansible-managed deployment projects](https://codebase.helmholtz.cloud/hifis-software-deployment/):

* [GitLab](https://codebase.helmholtz.cloud/hifis-software-deployment/gitlab)
* [Mattermost](https://codebase.helmholtz.cloud/hifis-software-deployment/mattermost)
* [Kroki](https://codebase.helmholtz.cloud/hifis-software-deployment/kroki)
* [HIFIS Helpdesk](https://codebase.helmholtz.cloud/hifis-software-deployment/helpdesk)

## Two-Factor Authentication (2FA) for GitLab and Mattermost

It is highly recommended to use
[Two-Factor Authentication (2FA)](https://en.wikipedia.org/wiki/Multi-factor_authentication)
in all offered HIFIS services that support 2FA and in particular for
authentication (which is the technical term for logging in into a service)
with GitLab and Mattermost.
2FA means that in addition to authenticating with your credentials (username and password) you use a further factor for proof-of-identity.
The second factor can be provided by devices like 2FA-mobile-apps or
USB-dongles.
Please read the following section about
[how to set up Two-Factor Authentication (2FA) in GitLab](concepts/2fa/index.md).

## Provided to you as part of HIFIS and [Helmholtz Cloud :fontawesome-solid-up-right-from-square:][cloud]

![LOGO HIFIS Software](../images/hifis_logo.svg)

**The services are administered by:**

<div style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
    <img alt="HZDR Logo" src="../images/hzdr_logo.svg">
    <img alt="UFZ Logo"  src="../images/ufz_logo.png">
</div>

As of today all services presented in this documentation are operated entirely
in the [HZDR](https://www.hzdr.de) data center.
They are administrated by colleagues from the
[HIFIS Software](https://hifis.net/services#software) team at
[HZDR](https://www.hzdr.de) and [UFZ](https://www.ufz.de).

[cloud]: https://helmholtz.cloud/
