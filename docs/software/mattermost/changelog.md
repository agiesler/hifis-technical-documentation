---
title: Service Changelog
---

## 2023-05-30 - Mattermost 7.10

* Update Mattermost to version 7.10 -
  [Release Post](https://mattermost.com/blog/mattermost-security-updates-7-10-1-7-9-4-7-8-5-esr-released/)
    * Channels: Set a reminder to read a message at a specific time
    * Channels: Mentions from muted channels are no longer shown on the browser tabs
    * Channels: Custom user status is now shown in the right-hand side Members pane
    * Channels: Invite multiple people at a time by email

## 2023-04-25 - Mattermost 7.9

* Update Mattermost to version 7.9 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-9-is-now-available/)
    * Boards: Team Administrator access to boards

## 2023-03-31 - Mattermost 7.8

* Update Mattermost to version 7.8 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-8-is-now-available/)
    * Boards: Improved filters and groups

## 2023-02-24 - Mattermost 7.7

* Update Mattermost to version 7.7 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-7-is-now-available/)
    * Channels: Mobile app v2 general availability
    * Channels: Message priority
    * Boards: File attachments
    * Boards: Sidebar drag and drop
    * Boards: Improved template picker
    * Playbooks: Run playbooks in an existing channel
    * Playbooks: Task Inbox

## 2023-01-03 - Mattermost 7.5

* Update Mattermost to version 7.5 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-5-is-now-available/)
    * Boards: Additional standard board templates
    * Boards: Filter by text properties
    * Channels: User last activity

## 2022-11-24 - Mattermost 7.4

* Update Mattermost to version 7.4 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-4-is-now-available/)
    * Boards: Additional board roles
    * Boards: Minimum default board roles
    * Boards: Add board members via autocomplete list
    * Boards: Channel notifications for linked boards
    * Boards: Multi-person property

## 2022-10-27 - Mattermost 7.3

* Update Mattermost to version 7.3 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-3-is-now-available/)
    * Boards: New role-based permissions system
    * Boards: New sidebar navigation
    * Boards: Link multiple boards to channels
    * Boards: Custom template permissions
    * Playbooks: Redesigned left-hand sidebar and run detail page

## 2022-10-04 - Mattermost 7.2

* Update Mattermost to version 7.2 coming with Message Forwarding -
  [Release Post](https://mattermost.com/blog/mattermost-v7-2-is-now-available/)

## 2022-08-25 - Mattermost 7.1

* Update Mattermost to version 7.1 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-1-is-now-available/)

## 2022-07-27

* Update Mattermost to version 7.0 -
  [Release Post](https://mattermost.com/blog/mattermost-v7-0-is-now-available/)
    * [Collapsed Reply Threads](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#collapsed) enabled by default
    * Completely redesigned [Message Formatting Toolbar](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#toolbar)
    * Multiple [Playbooks improvements](https://mattermost.com/blog/mattermost-v7-0-is-now-available/#inline)

## 2022-07-08

* Update Mattermost to version 6.7 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-7-is-now-available/)
    * Playbooks: [Set task due dates for playbook runs](https://mattermost.com/blog/mattermost-v6-7-is-now-available/#playbooks)

## 2022-06-21

* Update Mattermost to version 6.6 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-6-is-now-available/)
    * Channels: [Triggers and actions](https://mattermost.com/blog/mattermost-v6-6-is-now-available/#triggers)
    * Channels: [New location for message action](https://mattermost.com/blog/mattermost-v6-6-is-now-available/#message)

## 2022-04-26

* Update Mattermost to version 6.5 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-5-is-now-available/)
    * [Cross-team channel navigation](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#cross)
    * [Import, export, and duplicate Playbooks](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#playbooks)
    * [Improved share Boards UI](https://mattermost.com/blog/mattermost-v6-5-is-now-available/#share).
* Update Mattermost to version 6.4 -
[Release Post](https://mattermost.com/blog/mattermost-v6-4-is-now-available/)
    * Restriction of [one Playbook per team](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#playbooks) has been removed.
    * Multiple Boards enhancements including
        * new [standard templates](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#standard), 
        * new [template previews](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#template),
        * new [archive format with image support](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#archive), 
        * new [card badges](https://mattermost.com/blog/mattermost-v6-4-is-now-available/#badges).

## 2022-02-25

* Update Mattermost to version 6.3 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-3-is-now-available/)

## 2022-01-28

* Update Mattermost to version 6.2 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-2-is-now-available/)

## 2022-01-14

* Update Mattermost to version 6.1 -
  [Release Post](https://www.hifis.net/news/2022/01/14/mattermost-6-available)

## 2021-11-30

* Update Mattermost to version 5.39 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-39/)
    * Looking ahead to general availability of 
      [Collapsed Reply Threads](https://mattermost.com/blog/mattermost-v5-39/#collapsed)

## 2021-10-01

* Update Mattermost to version 5.38 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-38/)
    * Enhanced user 
      [onboarding experience](https://mattermost.com/blog/mattermost-v5-38/#onboarding)

## 2021-08-26

* Update Mattermost to version 5.37 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-37/)
    * Try out [Collapsed Reply Threads](https://mattermost.com/blog/collapsed-reply-threads-beta/)
    * Enable yourself in **Account Settings > Display > Collapsed Reply Threads (Beta)**.
