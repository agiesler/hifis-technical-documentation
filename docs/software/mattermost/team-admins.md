---
title: Getting Started for Team Admins
---

# Getting Started for Team Admins

[Team admins](https://docs.mattermost.com/messaging/cloud-user-management.html#team-admin)
are users who created a specific team or got assigned to a team as a team admin
by another team admin.
This user role enables users of specific teams in which they are team admins
to

* access the team settings, 
* change the team name, and
* access the manage members menu to control whether team members are a member 
or a team admin.

With the team admin role a few additional duties and responsibilities come into
play such as being responsible for the management of team members and their
roles.
This includes managing bots belonging to Mattermost plugins in your team so
that these bots can send bot notifications to team members using these
Mattermost plugins and their respective bots.

## Plugins, Bots and Bot Notifications

Many Mattermost users make use of the Mattermost plugins offered such as:

* Boards plugin, 
* CircleCI plugin,
* GitHub plugin,
* GitLab plugin,
* Matterpoll plugin,
* Playbooks plugin,
* Remind plugin,
* WelcomeBot plugin.

While users are able to use these plugins, at the time being you need to add
those bots belonging to these plugins to your team as team members to get
notifications from those bots.
This is because of the current restriction that only team members are allowed
to send notifications to other team members and not to users beyond the team, 
which is also a restriction for bots in Mattermost that are used by Mattermost
plugins.
If a bot is not part of the team, the bot is not allowed to send notifications
to team members.
This affects the following bots that are used by the respective Mattermost
plugins:

* Boards Bot,
* CircleCI Bot,
* GitHub Bot,
* GitLab Plugin Bot,
* Matterpoll Bot,
* Playbooks Bot,
* Remindbot Bot,
* Welcomebot Bot.
